apacheds Cookbook
=================

This cookbook installs and configure the Apache Directory Server.
It also provides convenience methods for adding entries in a LDAP server

Requirements
------------

#### packages
- `java` - apacheds needs java to run.
- `openssl` - required by the recipe for configuring certificates
- `ark` - required by the recipe to fetch Apache Directory Server binaries

Attributes
----------

This recipes depends on the __['install_from']['apache_mirror']__ attribute from the `ark` cookbook.

#### apacheds::server
Key                             | Type    | Description                                                                            | Default
--------------------------------|---------|----------------------------------------------------------------------------------------|---------------------------------------------------------------------
__['apacheds']['domain']__      | String  | The domain name of the Apache Directory Server                                         | __example.com__ if your node is __something.anywhere.example.com__
__['apacheds']['basedn']__      | String  | The base domain name of the Apache Directory Server                                    | __dc=example,dc=com__ if the domain attribute is __example.com__
__['apacheds']['version']__     | String  | The version of the Apache Directory Server to fetch                                    | __2.0.0-M15__
__['apacheds']['checksum']__    | String  | The checksum of the Apache Directory Server tarball to fetch                           | __9320a1374284af75875c7f895c0283bd282b2e8e16c65ad7d1ad19b6f80f4947__
__['apacheds']['instance']__    | String  | The Apache DS instance name                                                            | __default__
__['apacheds']['instance']__    | String  | The Apache DS instance name                                                            | __default__
__['apacheds']['port']__        | Integer | The port number on which the Apache Directory Server should serve LDAP requests        | __10389__
__['apacheds']['ssl_port']__    | Integer | The port number on which the Apache Directory Server should serve LDAPS requests       | __10636__
__['apacheds']['user']__        | String  | The user under which the Apache Directory Server run                                   | __apacheds__
__['apacheds']['group']__       | String  | The group under which the Apache Directory Server run                                  | __apacheds__
__['apacheds']['data_bag']__    | Hash    | Specifies the path to a data bag item to write                                         | __{ 'data_bag': 'servers', 'item': 'ldap' }__
__['apacheds']['certificate']__ | String  | The id of a certificate. A certificate is given in an item of data bag __certificates__. This item should contains the pem certificate in entry __certificate__, the certificate key in entry __key__ and the password of this key in entry __pass__  | __nil__
__['apacheds']['ssl_pass']__    | String  | The password of the key store in which SSL certificates are stored                     | __random__
__['apacheds']['config']__      | String  | A name of a file or a template in the cookbook to use for writing the config.ldif file | __nil__
__['apacheds']['root_dn']__     | String  | A root domain name for the default partition                                           | __dc=fr__
__['apacheds']['admin']__       | String  | The DN of the admin                                                                    | __uid=admin,dc=system__
__['apacheds']['defaultpw']__   | String  | The initial password of the admin account                                              | __secret__
__['apacheds']['password']__    | String  | The desired password for the admin account                                             | __secret__

Usage
-----
#### apacheds::default
Does nothing

#### apacheds::server
Installs and configures Apache Directory Server on the node.

e.g.
Just include `apacheds::server` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[apacheds::server]"
  ]
}
```

You can also sets the __['apacheds']['config']__ attribute to something and add the corresponding template to change the config.ldif file.

This recipe will write, if permission is given, the information about the LDAP server to the databag specified in the __['apacheds']['data_bag']__ attribute. This will enable to specify address and port number of your LDAP to all your servers.

#### apacheds_ldap_entry LWRP

This LWRP modifies an entry in the LDAP server. You can add an entry for instance by:
```ruby
apacheds_ldap_entry "cn=test,ou=people,dc=fr,dc=iqspot" do
  action :add
  host "ldap.iqspot.fr"
  bindDn "dc=fr,dc=iqspot,uid=admin"
  port 636
  password "secret"
  attributes ({ :objectclass => [ "inetOrgPerson", "top" ], :cn => "test" })
  ignore_auth_failure true
end
```

The possibles actions are:

* __add__ adds an entry 
* __update__ modifies an entry
* __replace_attributes__ replaces attributes
* __add_attributes__ add attributes
* __delete_attributes__ delete attributes

The various attributes of the LWRP are:

Attribute               | Type    | Description                                                                                                   | Default
------------------------|---------|---------------------------------------------------------------------------------------------------------------|-------------------------------------
__host__                | String  | The host name of the LDAP Server                                                                              | __localhost__
__bindDn__              | String  | The name of the DN to use to bind to the LDAP Server                                                          | __node['apacheds']['admin']__
__port__                | Integer | The port of the LDAP Server                                                                                   | __node['apacheds']['port']__
__password__            | String  | The password to bind to the LDAP Server                                                                       | __node['apacheds']['password']__
__attributes__          | Hash    | The list of attributes to modifies / sets. For deletion, the values of the hash are ignored                   | `{}`
__ignore_auth_failure__ | Boolean | Wether to ignore authentication failures or not (if true then an auth failure won't cause the recipe to fails | `false`


#### apacheds_ldap_schema LWRP

This LWRP modifies the schema of the LDAP server. The only available action for this is __append__ and can be used by insering the following code:
```ruby
apacheds_ldap_schema "schema.erb" do
  host "ldap.iqspot.fr"
  bindDn "dc=fr,dc=iqspot,uid=admin"
  port 636
  password "secret"
  variables ({"var1" => 1, "var2" => 2})
end
```

Where `schema.erb` is a ruby template that has access to the variables given in the __variables__ attribute. This template should return a list
of valid LDAP schema entry in the form of `entrytype (OOID NAME 'name')`.

The attributes of the LWRP are:

Attribute               | Type    | Description                                                                                                   | Default
------------------------|---------|---------------------------------------------------------------------------------------------------------------|-------------------------------------
__host__                | String  | The host name of the LDAP Server                                                                              | __localhost__
__bindDn__              | String  | The name of the DN to use to bind to the LDAP Server                                                          | __node['apacheds']['admin']__
__port__                | Integer | The port of the LDAP Server                                                                                   | __node['apacheds']['port']__
__password__            | String  | The password to bind to the LDAP Server                                                                       | __node['apacheds']['password']__
__variables__           | Hash    | The list of variables transmitted to the template for rendering                                               | `{}`

License and Authors
-------------------
Licence: Apache License version 2.0
Authors:

- Damien Martin-Guillerez <damien.martin-guillerez@inria.fr>
