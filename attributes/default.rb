default['install_from']['apache_mirror'] = 'http://apache.mirrors.multidist.eu/'
default['apacheds']['version'] = "2.0.0-M15"
default['apacheds']['checksum'] = "9320a1374284af75875c7f895c0283bd282b2e8e16c65ad7d1ad19b6f80f4947"
default['apacheds']['port'] = 10389
default['apacheds']['ssl_port'] = 10636
default['apacheds']['user'] = 'apacheds'
default['apacheds']['group'] = 'apacheds'
default['apacheds']['instance'] = 'default'
default['apacheds']['config'] = nil
default['apacheds']['data_bag'] = { 'data_bag' => 'servers', 'item' => 'ldap' }
default['apacheds']['root_dn'] = "dc=fr"
default['apacheds']['admin'] = "uid=admin,ou=system"
default['apacheds']['defaultpw'] = "secret"
