#
# Cookbook Name:: apacheds
#
# Copyright 2013, Inria
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#

def ldap_ssha(str)
  srand; salt = (rand * 1000).to_i.to_s 
  return '{SSHA}' + Base64.encode64(Digest::SHA1.digest(str + salt) + salt).chomp!
end
