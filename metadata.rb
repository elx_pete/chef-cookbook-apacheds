name             'apacheds'
maintainer       'iQSpot'
maintainer_email 'dev@iqspot.fr'
license          'Apache 2.0'
description      'Installs/Configures Apache Directory Server and provides facilities to access LDAP from Chef.'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '1.0.1'
supports 'ubuntu', ">= 12.04"
supports 'centos', ">= 0.0"

recipe           "apacheds", "Convenience recipe for inclusion, do nothing"
recipe           "apacheds::server", "Install and configure Apache Directory Server"

depends "java"
depends "openssl"
depends "ark"
