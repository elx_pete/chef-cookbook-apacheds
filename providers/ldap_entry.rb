#
# Cookbook Name:: apacheds
# Provider:: ldap_entry
#
# Copyright 2013, Inria
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
use_inline_resources if defined?(use_inline_resources)

def whyrun_supported?
  true
end

# determine the various params to connect to the ldap
def ldap_params(resource)
  defldap = {
    'host' => "localhost",
    'port' => node['apacheds']['port'],
    'bindDn' => node['apacheds']['admin'],
    'password' => node['apacheds']['password']
  }
  if node['apacheds']['data_bag'] then
    servers = data_bag(node['apacheds']['data_bag']['data_bag'])
    item = nil
    if servers and servers.include?(node['apacheds']['data_bag']['item']) then
      item = data_bag_item(node['apacheds']['data_bag']['data_bag'], node['apacheds']['data_bag']['item'])
      defldap["host"] = item["host"]
      defldap["port"] = item["port"]
      defldap["bindDn"] = item["binddn"]
      defldap["password"] = item["password"]
    end
  end

  res = {
    'bindDn'              => resource.bindDn || defldap["bindDn"],
    'password'            => resource.password || defldap["password"],
    'host'                => resource.host || defldap["host"],
    'port'                => resource.port || defldap["port"],
    'ignore_auth_failure' => resource.ignore_auth_failure || false
  }
  return res
end

# connect the LDAP
def ldap_entry_connect(resource)
  params = ldap_params(resource)
  ldap = Net::LDAP.new(:host => params['host'], :port => params['port'])
  ldap.auth params['bindDn'], params['password']
  begin
    if ldap.bind
      return ldap
    end
  rescue Net::LDAP::LdapError => e
    Chef::Log.info("LDAP error: #{e}")
  end
  if not params['ignore_auth_failure'] then
    Chef::Application.fatal!("Failed to bind to LDAP server: #{ldap.get_operation_result.message}")
  else
    Chef::Log.info("Failed to bind to LDAP server: #{ldap.get_operation_result.message}")
  end
  return nil
end

# Test if an LDAP entry exists
def ldap_entry_exists?(ldap, dn)
  begin
    if ldap.search(:base => dn, :scope => Net::LDAP::SearchScope_BaseObject, :return_result => false, :size => 1) then
      return true
    end
  rescue Net::LDAP::LdapError => ex
    Chef::Log.debug("Failed to search for entry #{dn}: #{ex}")
  end
  return false
end

# Get the value of a LDAP attribute
def ldap_attribute(ldap, dn, attributeName)
  attributes = [attributeName]
  Chef::Log.debug("ldap_attribute: Looking for dn #{dn}")
  begin
    ldap.search(:base => dn, :scope => Net::LDAP::SearchScope_BaseObject, :return_result => false, :size => 1, :attributes => attributes) do |entry|
      Chef::Log.debug("ldap_attribute: Found entry #{entry} for dn #{dn}")
      entry.each do |attr, value|
        return value
      end
    end
  rescue Net::LDAP::LdapError => ex
    Chef::Log.debug("ldap_attribute: Failed to look for attribute #{attributeName} of DN #{dn}: #{ex}")
  end
  return nil
end

# Get the map of all LDAP attributes
def ldap_attributes(ldap, dn)
  begin
    ldap.search(:base => dn, :scope => Net::LDAP::SearchScope_BaseObject, :return_result => false, :size => 1) do |entry|
      Chef::Log.debug("ldap_attribute: Found entry #{entry} for dn #{dn}")
      result = {}
      entry.each do |k,v|
        result[k] = v
      end
      return result
    end
  rescue Net::LDAP::LdapError => ex
    Chef::Log.debug("Failed to search for entry #{dn}: #{ex}")
  end
  return nil
end

# Compute the operations to go from old attributes to new attributes
def ldap_operation(old_attr, new_attr)
  ops = []
  old_attr.each do |k,v|
    if k != :dn then # ignore dn attribute
      if new_attr.key?(k) then
        v_new = new_attr[k]
        # Replacing single elements to array 
        if not v_new.kind_of?(Array) then
          v_new = [v_new]
        end
        if not v.kind_of?(Array) then
          v = [v]
        end
        # String conversion to avoid replacing numbers or other kind of attribute
        v = v.map { |x| "#{x}" }
        v_new = v_new.map { |x| "#{x}" }
        # Compares the two array
        if Hash[v.zip(v.map{|x| v.count(x)})] != Hash[v_new.zip(v_new.map{|x| v_new.count(x)})] then
          ops << [:replace, k, v_new]
        end
      else
        ops << [:delete, k, nil]
      end
    end
  end
  new_attr.each do |k,v|
    if not old_attr.key?(k) then
      ops << [:add, k, v]
    end
  end
  Chef::Log.debug "LDAP operations from #{old_attr} to #{new_attr} is #{ops}"
  return ops
end

# LDAP LDIF to use ldapmodify instead of ruby net/ldap which is erroneous
# Modify an entry
def ldap_modify_ldif(dn, ops)
  ldif = <<-EOH
dn: #{dn}
changetype: modify
EOH
  ops.each do |x|
    ldif << "#{x[0]}: #{x[1]}\n"
    if x[0] != :delete then
      if x[2].kind_of?(Array) then
        arr = x[2]
        ldif << "#{x[1]}: #{arr.shift}\n"
        arr.each do |v|
          ldif << "-\nadd: #{x[1]}\n#{x[1]}: #{v}\n"
        end
      else
        ldif << "#{x[1]}: #{x[2]}\n"
      end
    end
    ldif << "-\n"
  end
  return ldif
end

# Add an entry
def ldap_add_ldif(dn, attributes)
  ldif = <<-EOH
dn: #{dn}
changetype: add
EOH
  attributes.each do |attr,values|
    if values.kind_of?(Array) then
      values.each do |value|
        ldif << "#{attr}: #{value}\n"
      end
    else
        ldif << "#{attr}: #{values}\n"
    end
  end
  return ldif
end

# Remove an entry
def ldap_delete_ldif(dn)
  return <<-EOH
dn: #{dn}
changetype: delete
EOH
end

# Execute a ldif using ldapmodify
def ldap_execute(resource, ldif)
  attr = ldap_params(resource)
  result = `ldapmodify -h '#{attr['host']}' -p  #{attr['port']} -w '#{attr['password']}' -D '#{attr['bindDn']}' 2>&1 <<EOF
#{ldif}
EOF`
  if $?.exitstatus == 0 then
    Chef::Log.info "LDAP modification succeeded with output: #{result}" 
  else
    Chef::Application.fatal!("LDAP modification failed: #{result}")
  end
end

# and the actual things
def ldap_add(resource)
  ldap_execute(resource, ldap_add_ldif(resource.name, resource.attributes))
end
def ldap_modify(resource, ops)
  ldap_execute(resource, ldap_modify_ldif(resource.name, ops))
end
def ldap_delete(resource)
  ldap_execute(resource, ldap_delete_ldif(resource.name))
end


################
# Add an entry #
################
action :add do
  ldap = ldap_entry_connect new_resource
  if ldap then
    if ldap_entry_exists?(ldap, new_resource.name) then
      Chef::Log.info "#{new_resource} already exists - nothing to do"
    else
      converge_by "adding LDAP entry #{new_resource.name}" do
        ldap_add(new_resource)
      end
    end
  end
end

#####################################################################
# Update an entry, i.e. add or replace attributes if already exists #
#####################################################################
action :update do
  ldap = ldap_entry_connect new_resource
  if ldap then
    attr = ldap_attributes(ldap, new_resource.name)
    if attr != nil then
      ldap_ops = ldap_operation(attr, new_resource.attributes)
      if ldap_ops.empty? then
        Chef::Log.info "#{new_resource} is up to date - nothing to do"
      else
        converge_by "updating LDAP entry #{new_resource.name}" do
          ldap_modify(new_resource, ldap_ops)
        end
      end
    else
      converge_by "adding LDAP entry #{new_resource.name} [from update operation]" do
        ldap_add(new_resource)
      end
    end
  end
end

###################
# Delete an entry #
###################
action :delete do
  ldap = ldap_entry_connect new_resource
  if ldap then
    if not ldap_entry_exists?(ldap, new_resource.name) then
      Chef::Log.info "#{new_resource} does not exist - nothing to do"
    else
      converge_by "deleting LDAP entry #{new_resource.name}" do
        ldap_delete(new_resource)
      end
    end
  end
end

##########################################
# Update one or several entry attributes #
##########################################
def replace_attribute(new_resource)
  ldap = ldap_entry_connect new_resource
  if ldap then
    if not ldap_entry_exists?(ldap, new_resource.name) then
      Chef::Application.fatal!("LDAP entry #{new_resource} does not exists, cannot edit!")
    else
      ops = []
      new_resource.attributes.each do |key, value|
        oldvalue = ldap_attribute(ldap, new_resource.name, key)
        if oldvalue == nil then
          Chef::Application.fatal!("Attribute #{key} of LDAP entry #{new_resource.name} does not exists, cannot replace!")
        elsif oldvalue == value then
          Chef::Log.info "Attribute #{key} of LDAP entry #{new_resource} is up to date - nothing to do"
        else
          ops << [:replace, key, value]
        end
      end
      if not ops.empty? then
        converge_by "replacing attributes LDAP entry #{new_resource.name}" do
          ldap_modify(new_resource, ops)
        end
      end
    end
  end
end
action :replace_attribute do
  replace_attribute(new_resource)
end
action :replace_attributes do
  replace_attribute(new_resource)
end

##########################################
# Append one or several entry attributes #
##########################################
def add_attributes(new_resource)
  ldap = ldap_entry_connect new_resource
  if ldap then
    if not ldap_entry_exists?(ldap, new_resource.name) then
      Chef::Application.fatal!("LDAP entry #{new_resource} does not exists, cannot edit!")
    else
      ops = []
      new_resource.attributes.each do |key, value|
        oldvalue = ldap_attribute(ldap, new_resource.name, key)
        if oldvalue != nil then
          Chef::Log.info "Attribute #{key} of LDAP entry #{new_resource} already exists - nothing to do"
        else
          ops << [:add, key, value]
        end
      end
      if not ops.empty? then
        converge_by "adding attributes to LDAP entry #{new_resource.name}" do
          ldap_modify(new_resource, ops)
        end
      end
    end
  end
end
action :add_attributes do
  add_attributes(new_resource)
end
action :add_attribute do
  add_attributes(new_resource)
end

##########################################
# Remove one or several entry attribtues #
##########################################    
def delete_attributes(new_resource)
  ldap = ldap_entry_connect new_resource
  if ldap then
    if not ldap_entry_exists?(ldap, new_resource.name) then
      Chef::Application.fatal!("LDAP entry #{new_resource} does not exists, cannot edit!")
    else
      ops = []
      new_resource.attributes.each do |attr,v|
        oldvalue = ldap_attribute(ldap, new_resource.name, attr)
        if not oldvalue then
          Chef::Log.info "Attribute #{attr} of LDAP entry #{new_resource} does not exist - nothing to do"
        else
          ops << [:delete, attr, nil]
        end
      end
      if not ops.empty? then
        converge_by "deleting attributes of LDAP entry #{new_resource.name}" do
          ldap_modify(new_resource, ops)
        end
      end
    end
  end
end
action :delete_attributes do
  delete_attributes(new_resource)
end
action :delete_attribute do
  delete_attributes(new_resource)
end
