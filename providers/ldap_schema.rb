#
# Cookbook Name:: apacheds
# Provider:: ldap_schema
#
# Copyright 2013, Inria
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
use_inline_resources if defined?(use_inline_resources)

def whyrun_supported?
  true
end

def remove_comments(content)
  return content.gsub /^#.*$\n/, ''
end

def parse(content)
  s = remove_comments content
  return s.scan(/(?<type>\w+)\s+(?<value>\((?:(?>[^()]+)|\g<value>)*\))/)
end

#####################
# Add a LDAP schema #
#####################
action :append do
  ldap = ldap_entry_connect(new_resource)
  if ldap then
    schema = render new_resource.schema, :variables => new_resource.variables
    parse(schema).each do |t|
      ldap.add_attribute "cn=schema", t[0], t[1]
    end
  end
end
