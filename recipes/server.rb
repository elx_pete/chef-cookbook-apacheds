#
# Cookbook Name:: apacheds
# Recipe:: default
#
# Copyright 2013, Inria
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Install an Apache Directory Server
#

include_recipe 'java'
include_recipe 'ark'
include_recipe 'apacheds'

require 'fileutils'
require 'net/ldap'

pool =  [('a'..'z'),('A'..'Z')].map{|i| i.to_a}.flatten

fqdn = node['fqdn']
if node['set_fqdn'] then
  fqdn = node['set_fqdn']
end
domainName = node['apacheds']['domain']
if not domainName then
  domainName = fqdn.split(".")[-2,2].join(".")
end
baseDn = node['apacheds']['basedn']
if not baseDn then
  baseDn = domainName.split(".").map { |x| "dc=#{x}" }.join(",")
end

log "Installing Apache Directory Server on #{fqdn} for domain #{domainName} with Base DN #{baseDn}"
 
################################################################################
# Create the user
user node['apacheds']['user'] do
  shell '/bin/false'
  home "/home/#{node[:apacheds][:user]}"
  system true
  action :create
end
group node['apacheds']['group'] do
  members node['apacheds']['user']
  system true
  action :create
end

################################################################################
# Install the binaries
ark "apacheds" do
  action :install
  version node['apacheds']['version']
  url "#{node[:install_from][:apache_mirror]}/directory/apacheds/dist/#{node['apacheds']['version']}/apacheds-#{node['apacheds']['version']}.tar.gz"
  checksum node['apacheds']['checksum']
  strip_leading_dir true
  append_env_path false
  owner node['apacheds']['user']
  group node['apacheds']['group']
end

file "/usr/local/apacheds/bin/apacheds.sh" do # Exec flag is not present
  mode 0755
  action :touch
end

link "/etc/apacheds" do
  to "/usr/local/apacheds/instances/#{node[:apacheds][:instance]}/conf"
end

################################################################################
# Authorize well-known port using authbind
if node['apacheds']['port'] <= 1024 or node['apacheds']['ssl_port'] <= 1024 then
  package "authbind" do
    action :install
  end
  
  if node['apacheds']['port'] <= 1024 then
    file "/etc/authbind/byport/#{node['apacheds']['port']}" do
      user node['apacheds']['user']
      group node['apacheds']['group']
      mode "0700"
      action :touch
    end
  end
  if node['apacheds']['ssl_port'] <= 1024 then
    file "/etc/authbind/byport/#{node['apacheds']['ssl_port']}" do
      user node['apacheds']['user']
      group node['apacheds']['group']
      mode "0700"
      action :touch
    end
  end
end


password = (0...50).map{ pool[rand(pool.length)] }.join

################################################################################
# Records the links in the data bag item
if node['apacheds']['data_bag'] then
  servers = data_bag(node['apacheds']['data_bag']['data_bag'])
  item = nil
  if servers and servers.include?(node['apacheds']['data_bag']['item']) then
    item = data_bag_item(node['apacheds']['data_bag']['data_bag'], node['apacheds']['data_bag']['item'])
  end

  if item then
    if not item['host'] then
	item['host'] = fqdn
    end
    item['port'] = node['apacheds']['port']
    item['ssl_port'] = node['apacheds']['ssl_port']
    item['basedn'] = baseDn
    item['binddn'] = node['apacheds']['admin']
    if item['password'] then
      password = item['password']
    else
      item['password'] = password
    end
  else
    item = Chef::DataBagItem.new
    item.data_bag(node['apacheds']['data_bag']['data_bag'])
    item.raw_data = {
      "id" => node['apacheds']['data_bag']['item'],
      "port" => node['apacheds']['port'],
      "ssl_port" => node['apacheds']['ssl_port'],
      "basedn" => baseDn,
      "password" => password,
      "host" => fqdn,
      "binddn" => node['apacheds']['admin']
    }
  end
  begin
    item.save
  rescue Exception=>e
    log "Unable to save data bag, credentials probably missing"
  end
end

################################################################################
# Configure the LDAP admin password
attr = { :userPassword => ldap_ssha(password) }
apacheds_ldap_entry node['apacheds']['admin'] do
  bindDn node['apacheds']['admin']
  password node['apacheds']['defaultpw']
  host "localhost"
  port node['apacheds']['port']
  ignore_auth_failure true
  attributes attr
  action :replace_attribute
end

################################################################################
# Create the service
template "/etc/init.d/apacheds" do
  source "service.sh.erb"
  mode 0755
  action :create
end

service 'apacheds' do
  supports :status => true, :restart => true, :reload => false, :start => true, :stop => true
  action [ :enable, :start ]

  notifies :replace_attribute, resources(:apacheds_ldap_entry => node['apacheds']['admin']), :delayed
end

################################################################################
# Configure the certificates
conf_dir = "/usr/local/apacheds/instances/#{node[:apacheds][:instance]}/conf"
keystore = "#{conf_dir}/keystore"
if node[:apacheds][:ssl_pass]
  keystorepass = node[:apacheds][:ssl_pass]
else
  keystorepass =  (0...50).map{ pool[rand(pool.length)] }.join
end
node.set[:apacheds][:ssl_pass] = keystorepass

if node['apacheds']['certificate'] then
  ssl_cert = data_bag_item('certificates', node[:apacheds][:certificate])
else
  ssl_cert = nil
end
if ssl_cert then
  # Register the certificate in the keystore
  prefix = "#{conf_dir}/ssl"
  execute "apacheds-load-key" do
    command "rm -f #{keystore}; keytool -J-Xmx2m -importkeystore -srckeystore #{prefix}.pkcs12 -srcstoretype PKCS12 -destkeystore #{keystore} -srcstorepass '#{keystorepass}' -deststorepass '#{keystorepass}'"
    cwd conf_dir
    user node['apacheds']['user']
    group node['apacheds']['group']
    not_if { not ::File.exists?("#{prefix}.pkcs12") }
    action :nothing
    notifies :restart, "service[apacheds]", :delayed
  end
  execute "apacheds-pkcs12" do
    command "openssl pkcs12 -inkey #{prefix}.key -in #{prefix}.crt -export -out #{prefix}.pkcs12 -passout 'pass:#{keystorepass}'  -passin 'pass:#{ssl_cert['pass']}'"
    cwd conf_dir
    user node['apacheds']['user']
    group node['apacheds']['group']
    not_if { not ::File.exists?("#{prefix}.crt") }
    action :nothing
    notifies :run, "execute[apacheds-load-key]", :immediately
  end

  file "#{prefix}.key" do
    action :create
    content ssl_cert['key']
    user node['apacheds']['user']
    group node['apacheds']['group']
    mode "0600"
    notifies :run, "execute[apacheds-pkcs12]", :delayed
  end
  file "#{prefix}.crt" do
    action :create
    content ssl_cert['certificate']
    user node['apacheds']['user']
    group node['apacheds']['group']
    mode "0600"
    notifies :run, "execute[apacheds-pkcs12]", :delayed
  end
end

################################################################################
# Configure the desired instance
if node['apacheds']['config'] then
  begin
    params = {
      "baseDn" => baseDn,
      "domainName" => domainName,
      "fqdn" => fqdn,
      "keystorepass" => keystorepass,
      "keystore" => keystore,
      "rootDn" => node['apacheds']['root_dn'],
      "port" => node['apacheds']['port'],
      "ssl_port" => node['apacheds']['ssl_port']
    }
    template "/usr/local/apacheds/instances/#{node[:apacheds][:instance]}/conf/config.ldif" do
      source node['apacheds']['config']
      mode 0644
      action :create
      variables params
      owner node['apacheds']['user']
      group node['apacheds']['group']
      notifies :restart, "service[apacheds]", :delayed
    end
  rescue Chef::Exceptions::ResourceNotFound
    # If the template doesn't exists, we use the cookbook file
    cookbook_file "/usr/local/apacheds/instances/#{node[:apacheds][:instance]}/conf/config.ldif" do
      source "default-config.ldif.erb"
      mode 0644
      action :create
      owner node['apacheds']['user']
      group node['apacheds']['group']
      notifies :restart, "service[apacheds]", :delayed
    end
  end
end

