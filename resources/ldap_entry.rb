#
# Cookbook Name:: apacheds
# Resource:: ldap_entry
#
# Copyright 2013, Inria
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
actions :add, :update, :delete, :replace_attribute, :replace_attributes, :add_attribute, :add_attributes, :delete_attribute, :delete_attributes
default_action :add

attribute :name,                :name_attribute => true, :kind_of => String, :required => true
attribute :host,                :kind_of => String,          :default => nil
attribute :bindDn,              :kind_of => String,          :default => nil
attribute :attributes,          :kind_of => Hash,            :default => {}
attribute :port,                :kind_of => Fixnum,          :default => nil
attribute :password,            :kind_of => String,          :default => nil
attribute :ignore_auth_failure, :equal_to => [true, false],  :default => false
